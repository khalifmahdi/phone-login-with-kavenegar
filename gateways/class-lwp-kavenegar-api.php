<?php
/**
 * LWP_KaveNegar_Api class.
 *
 * The class names need to defined in format: LWP_%s_Api, where %s means Api name, e.g. LWP_KaveNegar_Api
 * The class methods need to end with api key name
 *
 * @package Login with phone number
 */

/**
 * Class LWP_KaveNegar_Api class.
 */
class LWP_KaveNegar_Api
{

    /**
     * Api Key
     *
     * @var string
     */
    public $API_key;
    public $template;

    /**
     * LWP_Handle_Messaging constructor.
     */
    public function __construct()
    {
        $options = get_option('idehweb_lwp_settings');

        if ( ! isset($options['idehweb_KaveNegar_API_Key']) ) {
			$options['idehweb_KaveNegar_API_Key'] = '';
		}

		if ( ! isset($options['idehweb_KaveNegar_template']) ) {
			$options['idehweb_KaveNegar_template'] = '';
		}

		$this->API_key	= $options['idehweb_KaveNegar_API_Key'];
        $this->template = $options['idehweb_KaveNegar_template'];
    }

    public function lwp_send_sms($phone, $code)
    {
		$url = "https://api.kavenegar.com/v1/{$this->API_key}/verify/lookup.json?receptor=$phone&token=$code&template={$this->template}";

        $response = wp_safe_remote_get(
			$url,
			[
				'method'	=> 'GET',
				'timeout'	=> 120,
				'blocking' 	=> true,
				'headers' 	=> [],
				'cookies' 	=> [],
			]
		);

        $user_message = '';
        $dev_message = array();
        $res_param = array();

        if ( is_wp_error($response) ) {
            $dev_message = $response->get_error_message();
            $success = false;
        } else {
            $decoded_response = json_decode($response['body'], true);
            $res_success = $decoded_response['return']['status'] == 200;
            $error_code = $decoded_response['return']['status'] != 200 ? $decoded_response['return']['status'] : '';

            if ($res_success) {
                $success = true;
            } elseif ('411' === $error_code) {
                $success = false;
                $user_message = __('Phone number is invalid', 'orion-login');
            } else {
                $success = false;
                $user_message = __("خطای ناشناخته ($error_code)", 'orion-login');
            }
        }

        return [
            'success' => $success,
            'userMessage' => $user_message,
            'devMessage' => $dev_message,
            'resParam' => $res_param,
        ];
    }
}